package io.gitlab.jfronny.szbnd;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;

import androidx.annotation.Nullable;

import java.util.Objects;

public class TransformActivity extends Activity {
    private static final String TAG = "TransformActivity";
    private static final String CONTENT_PREFIX = "content://io.gitlab.jfronny.szbnd.documents/document/";
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent intent = getIntent();
        if (intent != null && intent.getStringExtra("szbnd") != null) {
            finish();
            return;
        }
        assert intent != null;
        String url = Objects.requireNonNull(intent.getData()).toString();
        if (url.length() > 1 && url.endsWith("/")) url = url.substring(0, url.length() - 1);
        Log.d(TAG, "Got " + intent.getType() + " intent with URL=" + url);
        boolean forceChooser = false;
        if (url.startsWith(CONTENT_PREFIX)) {
            Log.w(TAG, "Caught intent intended for file manager. Attempting to prevent loop by opening app chooser");
            forceChooser = true;
            url = Uri.decode(url.substring(CONTENT_PREFIX.length()));
        } else {
            url = CONTENT_PREFIX + Uri.encode(url);
        }
        Log.d(TAG, url);
        Intent delegate = new Intent(Intent.ACTION_VIEW);
        delegate.setDataAndType(Uri.parse(url), "vnd.android.document/directory");
        delegate.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        if (forceChooser) Intent.createChooser(delegate, getResources().getString(R.string.app_chooser));
        startActivity(delegate);
        finish();
    }
}
