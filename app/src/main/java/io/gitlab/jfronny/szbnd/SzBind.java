package io.gitlab.jfronny.szbnd;

import android.content.res.AssetFileDescriptor;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.graphics.Point;
import android.os.CancellationSignal;
import android.os.ParcelFileDescriptor;
import android.provider.DocumentsContract;
import android.provider.DocumentsContract.Document;
import android.provider.DocumentsContract.Root;
import android.provider.DocumentsProvider;
import android.util.Log;
import android.webkit.MimeTypeMap;

import androidx.annotation.Nullable;

import com.topjohnwu.superuser.Shell;
import com.topjohnwu.superuser.io.SuFile;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

public class SzBind extends DocumentsProvider {
    private static final String TAG = "SzBind";
    private static final String ALL_MIME_TYPES = "*/*";
    public static final String ROOT_DIR = "/";

    // The default columns to return information about a root if no specific
    // columns are requested in a query.
    private static final String[] DEFAULT_ROOT_PROJECTION = new String[] {
            Root.COLUMN_ROOT_ID,
            Root.COLUMN_MIME_TYPES,
            Root.COLUMN_FLAGS,
            Root.COLUMN_ICON,
            Root.COLUMN_TITLE,
            Root.COLUMN_SUMMARY,
            Root.COLUMN_DOCUMENT_ID,
            Root.COLUMN_AVAILABLE_BYTES
    };

    // The default columns to return information about a document if no specific
    // columns are requested in a query.
    private static final String[] DEFAULT_DOCUMENT_PROJECTION = new String[] {
            Document.COLUMN_DOCUMENT_ID,
            Document.COLUMN_MIME_TYPE,
            Document.COLUMN_DISPLAY_NAME,
            Document.COLUMN_LAST_MODIFIED,
            Document.COLUMN_FLAGS,
            Document.COLUMN_SIZE
    };

    @Override
    public Cursor queryRoots(String[] projection) throws FileNotFoundException {
        Log.d(TAG, "queryRoots");
        final MatrixCursor result = new MatrixCursor(projection != null ? projection : DEFAULT_ROOT_PROJECTION);

        if (!Shell.rootAccess()) return result;

        final MatrixCursor.RowBuilder row = result.newRow();
        row.add(Root.COLUMN_ROOT_ID, ROOT_DIR);
        row.add(Root.COLUMN_DOCUMENT_ID, ROOT_DIR);
        row.add(Root.COLUMN_SUMMARY, null);
        row.add(Root.COLUMN_FLAGS, Root.FLAG_SUPPORTS_CREATE | Root.FLAG_SUPPORTS_IS_CHILD | Root.FLAG_SUPPORTS_SEARCH);
        row.add(Root.COLUMN_TITLE, getContext().getString(R.string.root_summary));
        row.add(Root.COLUMN_MIME_TYPES, ALL_MIME_TYPES);
        row.add(Root.COLUMN_AVAILABLE_BYTES, new SuFile(ROOT_DIR).getFreeSpace());
        row.add(Root.COLUMN_ICON, R.mipmap.ic_launcher);
        Log.d(TAG, "queryRoots: success");

        return result;
    }

    @Override
    public Cursor queryDocument(String documentId, String[] projection) throws FileNotFoundException {
        Log.d(TAG, "queryDocument");
        projection = projection != null ? projection : DEFAULT_DOCUMENT_PROJECTION;
        Set<String> setProjection = new HashSet<>(Arrays.asList(projection));
        final MatrixCursor result = new MatrixCursor(projection);
        includeFile(result, documentId, null, setProjection);
        Log.d(TAG, "queryDocument: success");
        return result;
    }

    @Override
    public Cursor queryChildDocuments(String parentDocumentId, String[] projection, String sortOrder) throws FileNotFoundException {
        Log.d(TAG, "queryChildDocuments: " + parentDocumentId);
        projection = projection != null ? projection : DEFAULT_DOCUMENT_PROJECTION;
        Set<String> setProjection = new HashSet<>(Arrays.asList(projection));
        final MatrixCursor result = new MatrixCursor(projection);
        final File parent = getFileForDocId(parentDocumentId);
        for (File file : parent.listFiles()) {
            includeFile(result, null, file, setProjection);
        }
        Log.d(TAG, "queryChildDocuments: success");
        return result;
    }

    @Override
    public ParcelFileDescriptor openDocument(String documentId, String mode, @Nullable CancellationSignal signal) throws FileNotFoundException {
        Log.d(TAG, "openDocument");
        final File file = getFileForDocId(documentId);
        final int accessMode = ParcelFileDescriptor.parseMode(mode);
        Log.d(TAG, "openDocument: success");
        return ParcelFileDescriptor.open(file, accessMode);
    }

    @Override
    public AssetFileDescriptor openDocumentThumbnail(String documentId, Point sizeHint, CancellationSignal signal) throws FileNotFoundException {
        Log.d(TAG, "openDocumentThumbnail");
        final File file = getFileForDocId(documentId);
        final ParcelFileDescriptor pfd = ParcelFileDescriptor.open(file, ParcelFileDescriptor.MODE_READ_ONLY);
        Log.d(TAG, "openDocumentThumbnail: success");
        return new AssetFileDescriptor(pfd, 0, file.length());
    }

    @Override
    public boolean onCreate() {
        return true;
    }

    @Override
    public String createDocument(String parentDocumentId, String mimeType, String displayName) throws FileNotFoundException {
        Log.d(TAG, "createDocument");
        File newFile = new SuFile(parentDocumentId, displayName);
        int noConflictId = 2;
        while (newFile.exists()) {
            newFile = new SuFile(parentDocumentId, displayName + " (" + noConflictId++ + ")");
        }
        try {
            boolean succeeded;
            if (Document.MIME_TYPE_DIR.equals(mimeType)) {
                succeeded = newFile.mkdir();
            } else {
                succeeded = newFile.createNewFile();
            }
            if (!succeeded) {
                throw new FileNotFoundException("Failed to create document with id " + newFile.getPath());
            }
        } catch (IOException e) {
            throw new FileNotFoundException("Failed to create document with id " + newFile.getPath());
        }
        Log.d(TAG, "createDocument: success");
        return newFile.getPath();
    }

    @Override
    public void deleteDocument(String documentId) throws FileNotFoundException {
        Log.d(TAG, "createDocument");
        SuFile file = getFileForDocId(documentId);
        if (!file.deleteRecursive()) {
            throw new FileNotFoundException("Failed to delete document with id " + documentId);
        }
        Log.d(TAG, "createDocument: success");
    }

    @Override
    public String getDocumentType(String documentId) throws FileNotFoundException {
        Log.d(TAG, "getDocumentType");
        File file = getFileForDocId(documentId);
        return file.isDirectory() ? Document.MIME_TYPE_DIR : getMimeType(file);
    }

    @Override
    public Cursor querySearchDocuments(String rootId, String query, String[] projection) throws FileNotFoundException {
        Log.d(TAG, "querySearchDocuments");
        projection = projection != null ? projection : DEFAULT_DOCUMENT_PROJECTION;
        Set<String> setProjection = new HashSet<>(Arrays.asList(projection));
        final MatrixCursor result = new MatrixCursor(projection);
        final File parent = getFileForDocId(rootId);

        // This example implementation searches file names for the query and doesn't rank search
        // results, so we can stop as soon as we find a sufficient number of matches.  Other
        // implementations might rank results and use other data about files, rather than the file
        // name, to produce a match.
        final LinkedList<File> pending = new LinkedList<>();
        pending.add(parent);

        final int MAX_SEARCH_RESULTS = 50;
        while (!pending.isEmpty() && result.getCount() < MAX_SEARCH_RESULTS) {
            final File file = pending.removeFirst();
            if (file.isDirectory()) {
                Collections.addAll(pending, file.listFiles());
            } else {
                if (file.getName().toLowerCase().contains(query)) {
                    includeFile(result, null, file, setProjection);
                }
            }
        }
        Log.d(TAG, "querySearchDocuments: success");

        return result;
    }

    @Override
    public boolean isChildDocument(String parentDocumentId, String documentId) {
        return documentId.startsWith(parentDocumentId);
    }

    @Override
    public DocumentsContract.Path findDocumentPath(@Nullable String parentDocumentId, String childDocumentId) throws FileNotFoundException {
        Log.d(TAG, "findDocumentPath parent=" + parentDocumentId + " child=" + childDocumentId);
        // Basic sanity checks
        if (!childDocumentId.startsWith("/")) throw new FileNotFoundException();
        if (childDocumentId.length() > 1 && childDocumentId.endsWith("/")) throw new FileNotFoundException();
        if (parentDocumentId != null) {
            if (!parentDocumentId.startsWith("/")) throw new FileNotFoundException();
            if (parentDocumentId.length() > 1 && parentDocumentId.endsWith("/")) throw new FileNotFoundException();
            if (!childDocumentId.startsWith(parentDocumentId)) throw new FileNotFoundException();
        }
        // Generate the path
        return new DocumentsContract.Path(parentDocumentId == null ? ROOT_DIR : null, findDocumentPathI(parentDocumentId, childDocumentId));
    }

    // Naive implementation for findDocumentPath, gets paths between parentDocumentId and childDocumentId
    private static List<String> findDocumentPathI(@Nullable String parentDocumentId, String childDocumentId) {
        if (parentDocumentId == null) parentDocumentId = "/";
        List<String> path = new LinkedList<>();
        path.add(parentDocumentId);
        if (parentDocumentId.equals(childDocumentId)) return path;
        childDocumentId = childDocumentId.substring(1);
        if (parentDocumentId.length() > 1) childDocumentId = childDocumentId.substring(parentDocumentId.length());
        if (parentDocumentId.endsWith("/")) parentDocumentId = parentDocumentId.substring(0, parentDocumentId.length() - 1);
        StringBuilder sb = new StringBuilder(parentDocumentId);
        for (String s : childDocumentId.split("/")) {
            path.add(sb.append('/').append(s).toString());
        }
        return path;
    }

    /**
     * Get the document id given a file. This document id must be consistent across time as other
     * applications may save the ID and use it to reference documents later.
     * <p/>
     * The reverse of @{link #getFileForDocId}.
     */
    private static String getDocIdForFile(File file) {
        return file.getAbsolutePath();
    }

    /**
     * Get the file given a document id (the reverse of {@link #getDocIdForFile(File)}).
     */
    private static SuFile getFileForDocId(String docId) throws FileNotFoundException {
        final SuFile f = new SuFile(docId);
        if (!f.exists()) throw new FileNotFoundException(f.getAbsolutePath() + " not found");
        return f;
    }

    private static String getMimeType(File file) {
        if (file.isDirectory()) {
            return Document.MIME_TYPE_DIR;
        } else {
            final String name = file.getName();
            final int lastDot = name.lastIndexOf('.');
            if (lastDot >= 0) {
                final String extension = name.substring(lastDot + 1).toLowerCase();
                final String mime = MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension);
                if (mime != null) return mime;
            }
            return "application/octet-stream";
        }
    }

    /**
     * Add a representation of a file to a cursor.
     *
     * @param result the cursor to modify
     * @param docId  the document ID representing the desired file (may be null if given file)
     * @param file   the File object representing the desired file (may be null if given docID)
     */
    private void includeFile(MatrixCursor result, String docId, File file, Set<String> projection) throws FileNotFoundException {
        long start = System.currentTimeMillis();

        final MatrixCursor.RowBuilder row = result.newRow();

        if (docId == null) docId = getDocIdForFile(file);
        else file = getFileForDocId(docId);

        boolean mimeRequested = projection.contains(Document.COLUMN_MIME_TYPE);
        boolean flagsRequested = projection.contains(Document.COLUMN_FLAGS);

        boolean isDirectory = (mimeRequested || flagsRequested) && file.isDirectory();
        final String mimeType = mimeRequested || flagsRequested ? (isDirectory ? Document.MIME_TYPE_DIR : getMimeType(file)) : null;
        if (mimeRequested) row.add(Document.COLUMN_MIME_TYPE, mimeType);
        if (flagsRequested) {
            int flags = 0;
            if (mimeType.startsWith("image/")) flags |= Document.FLAG_SUPPORTS_THUMBNAIL;
            if (file.canWrite()) flags |= isDirectory ? Document.FLAG_DIR_SUPPORTS_CREATE : Document.FLAG_SUPPORTS_WRITE;
            File parent = file.getParentFile();
            if (parent != null && parent.canWrite()) flags |= Document.FLAG_SUPPORTS_DELETE;
            row.add(Document.COLUMN_FLAGS, flags);
        }

        row.add(Document.COLUMN_DOCUMENT_ID, docId);
        row.add(Document.COLUMN_DISPLAY_NAME, file.getName());
        if (projection.contains(Document.COLUMN_SIZE)) row.add(Document.COLUMN_SIZE, file.length());
        if (projection.contains(Document.COLUMN_LAST_MODIFIED)) row.add(Document.COLUMN_LAST_MODIFIED, file.lastModified());
        if (projection.contains(Document.COLUMN_ICON)) row.add(Document.COLUMN_ICON, R.mipmap.ic_launcher);

        Log.d(TAG, "Built row in " + (System.currentTimeMillis() - start) + "ms");
    }
}
